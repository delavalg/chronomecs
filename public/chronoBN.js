// Genre intervenant.e
var F = 0;
var H = 1;
var NA = -1;

// Infos suppl. intervenant.e
var D = 2;
var BN = 3;

// Event types
var EVT_DEBUT_PAROLE = 0;
var EVT_FIN_PAROLE = 1;
var EVT_INTERRUPTION = 2;
var EVT_INTER_COURTE = 3
var EVT_REPRISE = 4;
var EVT_START = 5;
var EVT_STOP = 6;
var EVT_PAUSE = 7;

// Etats
var ETAT_STOP = 0;
var ETAT_STARTED = 1;
var ETAT_PAUSE = 2;

// Events
function Event(typeEvent, genreEvent) {
    this.timeStamp = new Date();
    this.typeEvent = typeEvent;
    this.genreEvent = genreEvent;
};

function State(controls, view) {
    this.controls = controls;
    this.view = view;

    this.events = [];
    
    this.timeBegin = 0;
    this.timeStop = 0;
    this.timePause = 0;
    
    // Nombre de femmes
    this.nbFemmes = 0;
    // Nombre d'hommes
    this.nbHommes = 0;

    // Nombre d'interventions
    // Par genre (indices F = 0 et H = 1)
    // Par catégories (indices D = 3 et BN = 4)
    this.nbInterv = [0, 0, 0, 0];
    // Temps global de parole (en secondes)
    this.globalTime = [0, 0, 0, 0];
    // Nombre d'interruptions
    // nbInterrupt[X] = Nb de X interrompues
    this.nbInterrupt = [0, 0];
    this.nbInterruptCourte = [0, 0];
    // Interruption par
    // nbInterruptPar[X][Y] = Nb de X interrompues par Y
    this.nbInterruptPar = [[0, 0], [0, 0]];
    // Nombre de reprises de paroles apres interruptions
    this.nbReprises = [0, 0];
    // Genre de celui/celle qui a la parole
    this.genreCourant = NA;
    // Info suppl de celui/celle qui a la parole
    this.infoCourant = NA;
    // Temps de parole courant
    this.currentTime = 0;
    // Etat courant
    this.etat = ETAT_STOP;
}

function reset(state) {
    state.events = [];
    
    state.timeBegin = 0;
    state.timeStop = 0;
    state.timePause = 0;
    
    // Tableau du nombre d'interventions :
    // Par genre (indices F = 0 et H = 1)
    // Par catégories (indices D = 3 et BN = 4)
    state.nbInterv = [0, 0, 0, 0];
    // Tableau du temps global d'interventions
    state.globalTime = [0, 0, 0, 0];
    state.nbInterrupt = [0, 0];
    state.nbInterruptCourte = [0, 0];
    state.nbInterruptPar = [[0, 0], [0, 0]];
    state.nbReprises = [0, 0];
    state.genreCourant = NA;
    state.infoCourant = NA;
    state.currentTime = 0;
    state.etat = ETAT_STOP;
}

function timeView(nbs) {
    if (isNaN(nbs)) {
        return "-";
    } else {
        var d = new Date(2017,1,1,0,0,nbs);
        return d.toLocaleTimeString().replace(/ [A-Z]+$/, "");
    }
}

function percent(nb,total) {
    if (total == 0) {
        return "-";
    } else {
        return (nb*100/total).toFixed(2) + "%";
    }
}

function updateNbFH(state) {
    var nbF = parseInt(state.controls.inputNbFemmes.value);
    var nbH = parseInt(state.controls.inputNbHommes.value);
    var nbD = parseInt(state.controls.inputNbDelegue.value);
    var nbBN = parseInt(state.controls.inputNbBN.value);
    state.nbFemmes = nbF;
    state.nbHommes = nbH;
    state.nbDelegue = nbD;
    state.nbBN = nbBN;
    state.view.nbF.innerHTML = nbF;
    state.view.nbH.innerHTML = nbH;
    state.view.pourcentageF.innerHTML = percent(nbF, nbF+nbH);
    state.view.pourcentageH.innerHTML = percent(nbH, nbF+nbH);
    state.view.nbD.innerHTML = nbD;
    state.view.nbBN.innerHTML = nbBN;
    state.view.pourcentageD.innerHTML = percent(nbD, nbD+nbBN);
    state.view.pourcentageBN.innerHTML = percent(nbBN, nbD+nbBN);
    state.view.nbT.innerHTML = nbF + nbH;
}

function updateView(state) {
    var nbF = state.nbInterv[F];
    var nbH = state.nbInterv[H];
    var nbD = state.nbInterv[D];
    var nbBN = state.nbInterv[BN];
    var tot = nbF + nbH;
    state.view.nbIntervF.innerHTML = nbF;
    state.view.nbIntervH.innerHTML = nbH;
    state.view.pourcentageIntervF.innerHTML = percent(nbF,tot);
    state.view.pourcentageIntervH.innerHTML = percent(nbH,tot);
    state.view.nbIntervD.innerHTML = nbD;
    state.view.nbIntervBN.innerHTML = nbBN;
    state.view.pourcentageIntervD.innerHTML = percent(nbD,nbD+nbBN);
    state.view.pourcentageIntervBN.innerHTML = percent(nbBN,nbD+nbBN);
    state.view.nbIntervTotal.innerHTML = tot;
    var gtf = state.globalTime[F];
    var gth = state.globalTime[H];
    var gtd = state.globalTime[D];
    var gtbn = state.globalTime[BN];
    var gt = gtf + gth;
    state.view.globalTimeF.innerHTML = timeView(gtf);
    state.view.globalTimeH.innerHTML = timeView(gth);
    state.view.pourcentageTimeF.innerHTML = percent(gtf,gt);
    state.view.pourcentageTimeH.innerHTML = percent(gth,gt);
    state.view.globalTimeD.innerHTML = timeView(gtd);
    state.view.globalTimeBN.innerHTML = timeView(gtbn);
    state.view.pourcentageTimeD.innerHTML = percent(gtd,gtd+gtbn);
    state.view.pourcentageTimeBN.innerHTML = percent(gtbn,gtd+gtbn);
    state.view.globalTime.innerHTML = timeView(gt);
    state.view.avgTimeF.innerHTML = timeView(gtf/nbF);
    state.view.avgTimeH.innerHTML = timeView(gth/nbH);
    state.view.avgTimeD.innerHTML = timeView(gtd/nbD);
    state.view.avgTimeBN.innerHTML = timeView(gtbn/nbBN);
    state.view.avgTime.innerHTML = timeView(gt/tot);
    var nbIF = state.nbInterrupt[F] + state.nbInterruptCourte[F];
    var nbIH = state.nbInterrupt[H] + state.nbInterruptCourte[H];
    var totI = nbIF + nbIH;
    state.view.nbInterruptF.innerHTML = nbIF;
    state.view.nbInterruptH.innerHTML = nbIH;
    state.view.nbInterruptT.innerHTML = totI;
    state.view.pourcentageInterruptF.innerHTML = percent(nbIF,totI);
    state.view.pourcentageInterruptH.innerHTML = percent(nbIH,totI);
    var nbIntFF = state.nbInterruptPar[F][F];
    var nbIntFH = state.nbInterruptPar[F][H];
    var nbIntHF = state.nbInterruptPar[H][F];
    var nbIntHH = state.nbInterruptPar[H][H];
    state.view.nbIntFF.innerHTML = nbIntFF;
    state.view.nbIntFH.innerHTML = nbIntFH;
    state.view.nbIntHF.innerHTML = nbIntHF;
    state.view.nbIntHH.innerHTML = nbIntHH;
    state.view.pcIntFF.innerHTML = percent(nbIntFF,nbIF);
    state.view.pcIntFH.innerHTML = percent(nbIntFH,nbIF);
    state.view.pcIntHF.innerHTML = percent(nbIntHF,nbIH);
    state.view.pcIntHH.innerHTML = percent(nbIntHH,nbIH);
    var nbRF = state.nbReprises[F];
    var nbRH = state.nbReprises[H];
    var totR = nbRF + nbRH;
    state.view.nbReprisesF.innerHTML = nbRF;
    state.view.nbReprisesH.innerHTML = nbRH;
    state.view.nbReprisesT.innerHTML = totR;
    state.view.pcReprisesF.innerHTML = percent(nbRF,state.nbInterrupt[F]);
    state.view.pcReprisesH.innerHTML = percent(nbRH,state.nbInterrupt[H]);
};
    
function updateTime(state) {
    // assume started
    if (state.genreCourant != NA) {
        state.globalTime[state.genreCourant] += 1;
    }
    if (state.infoCourant != NA) {
        state.globalTime[state.infoCourant] += 1;
    }
    state.currentTime += 1;
    updateView(state);
};

// Identifier la catégorie de l'intervenant courant
// à partir de l'état des boutons
// Mise à jour de l'état en fonction
function getInfoCourant(state) {
    if (state.controls.inputDelegue.checked) {
        state.infoCourant = D;
    } else if (state.controls.inputBN.checked) {
        state.infoCourant = BN;
    } else {
        state.infoCourant = NA;
    }
}

function updateInfoIntervCourant(state) {
    var precInfoCourant = state.infoCourant;

    getInfoCourant(state);

    if (state.infoCourant != precInfoCourant) {
        // Changement d'info
        // Mise à jour de l'état
        if (precInfoCourant != NA) {
            // On enlève 1 intervention pour l'info précédente
            state.nbInterv[precInfoCourant] -= 1;
            state.globalTime[precInfoCourant] -= state.currentTime;
        }
        if (state.infoCourant != NA) {
            // On rajoute une intervention pour la nouvelle info
            state.nbInterv[state.infoCourant] += 1;
            state.globalTime[state.infoCourant] += state.currentTime;
        }
    }
    updateView(state);
}

function evtStart(state) {
    state.etat = ETAT_STARTED;
    for (c in state.controls) {
        state.controls[c].disabled = false;
    }
    state.controls.buttonStart.disabled = true;
    state.timer = setInterval(function(){updateTime(state);}, 1000);
    updateNbFH(state);
    updateView(state);
};

function evtPause(state) {
    state.etat = ETAT_PAUSE;
    for (c in state.controls) {
        state.controls[c].disabled = true;
    }
    state.controls.buttonStart.disabled = false;
    state.controls.buttonReset.disabled = false;
    state.controls.buttonExportView.disabled = false;
    state.controls.buttonExportTrace.disabled = false;
    state.controls.debutParoleF.disabled = false;
    state.controls.debutParoleH.disabled = false;
    clearInterval(state.timer);
}

function evtStop(state) {
    state.etat = ETAT_STOP;
    for (c in state.controls) {
        state.controls[c].disabled = true;
    }
    state.controls.buttonStart.disabled = false;
    state.controls.buttonReset.disabled = false;
    state.controls.buttonExportView.disabled = false;
    state.controls.buttonExportTrace.disabled = false;
    state.controls.debutParoleF.disabled = false;
    state.controls.debutParoleH.disabled = false;
    clearInterval(state.timer);
}

function evtReset(state) {
    state.etat = ETAT_STOP;
    for (c in state.controls) {
        state.controls[c].disabled = true;
    }
    state.controls.buttonStart.disabled = false;
    state.controls.buttonReset.disabled = false;
    state.controls.buttonExportView.disabled = false;
    state.controls.buttonExportTrace.disabled = false;
    state.controls.debutParoleF.disabled = false;
    state.controls.debutParoleH.disabled = false;
    clearInterval(state.timer);
    reset(state);
    updateView(state);
}

function handleEvent(state, typeEvent, genreEvent) {
    var evt = new Event(typeEvent, genreEvent);
    state.events.push(evt);
    return evt;
}

function debutParole(state, genre) {
    var evt = handleEvent(state, EVT_DEBUT_PAROLE, genre);
    if (state.etat != ETAT_STARTED) {
        evtStart(state);
    } 
    state.controls.repriseParoleF.disabled = true;
    state.controls.repriseParoleH.disabled = true;
    state.genreCourant = genre;
    state.nbInterv[genre] += 1;
    getInfoCourant(state);
    if (state.infoCourant != NA) {
        state.nbInterv[state.infoCourant] += 1;
    }
    state.currentTime = 0;
    updateView(state);
}

function finParole(state) {
    var evt = handleEvent(state, EVT_FIN_PAROLE, state.genrecourant);
    state.genreCourant = NA;
    state.infoCourant = NA;
    state.currentTime = 0;
    updateView(state);
}

function interParole(state, genre) {
    var evt = handleEvent(state, EVT_INTERRUPTION, genre);
    if (state.genreCourant != NA) {
        state.nbInterrupt[state.genreCourant] += 1;
        state.nbInterruptPar[state.genreCourant][genre] += 1;
    }
    if (state.genreCourant == F) {
        state.controls.repriseParoleF.disabled = false;
    }
    if (state.genreCourant == H) {
        state.controls.repriseParoleH.disabled = false;
    }
    state.interruptedTime = state.currentTime;
    state.currentTime = 0;
    state.genreCourant = genre;
    state.nbInterv[genre] += 1;
    if (state.infoCourant != NA) {
        state.nbInterv[state.infoCourant] += 1;
    }
    updateView(state);
}

function interCourte(state, genre) {
    var evt = handleEvent(state, EVT_INTER_COURTE, genre);
    if (state.genreCourant != NA) {
        state.nbInterruptCourte[state.genreCourant] += 1;
        state.nbInterruptPar[state.genreCourant][genre] += 1;
    }
    updateView(state);
}

function repriseParole(state, genre) {
    var evt = handleEvent(state, EVT_REPRISE, genre);
    state.nbReprises[genre] += 1;
    state.genreCourant = genre;
    state.currentTime = state.interruptedTime;
    state.controls.repriseParoleF.disabled = true;
    state.controls.repriseParoleH.disabled = true;
    // Pas d'incrementation du nb d'interventions pour une reprise
    // Normalement c'est la meme intervention
    updateView(state);
}

var textFile = null;
function makeTextFile(text, mimetype) {
    var data = new Blob([text], {type: mimetype});

    // If we are replacing a previously generated file we need to
    // manually revoke the object URL to avoid memory leaks.
    if (textFile !== null) {
        window.URL.revokeObjectURL(textFile);
    }
    
    textFile = window.URL.createObjectURL(data);
    
    return textFile;
};


function exportText(text, filename, mimetype) {
    var link = document.createElement('a');
    link.setAttribute('download', filename);
    link.href = makeTextFile(text, mimetype);
    document.body.appendChild(link);

    // wait for the link to be added to the document
    window.requestAnimationFrame(function () {
        var event = new MouseEvent('click');
        link.dispatchEvent(event);
        document.body.removeChild(link);
    });
}

function exportView() {
    var header = "\
<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n\
                      \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n\
<html>\n\
  <head>\n\
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\n\
    <title></title>\n\
  </head>\n\
  <style>\n\
      table {\n\
      align: center;\n\
      border-collapse: collapse;\n\
      }\n\
      td, th {\n\
      border: 1px solid black;\n\
      padding: 2mm;\n\
      text-align: center;\n\
      }\n\
  </style>\n\
<body>\n";
    var body = document.getElementById("view").innerHTML;
    var footer = "</body>\n</html>";
    var text = header + body + footer;
    exportText(text, "Tableau - " + Date() + ".html", "text/html");
}

function exportTrace(state) {
    var text = JSON.stringify(state.events);
    exportText(text, "Trace - " + Date() + ".json", "application/json");
}
